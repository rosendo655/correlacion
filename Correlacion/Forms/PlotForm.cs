﻿using Correlacion.Control;
using Correlacion.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Correlacion.Forms
{
    public partial class PlotForm : Form
    {
        public PlotForm()
        {
            InitializeComponent();
        }

        public PlotForm(Tabla<float> tableToPlot, IEnumerable<Columna> columnas, FormulaCorrelacion formula,IMetodoRegresion funcion) : this()
        {
            var funcionRegresion = funcion.FuncionRegresion(tableToPlot, columnas);
            Func<float, float> funcionGrafica = funcionRegresion.PlotFunction;
            
            float factor = formula.CalculoIndiceRelacion(tableToPlot, columnas);

            float error = Calculos.Error(tableToPlot, columnas, funcionGrafica);

            plotView.Model = PlotModelGenerator.ScatterModel(tableToPlot, columnas, funcionRegresion);

            lbl_coeficiente.Text = $"{factor:0.0000}";
            lbl_error.Text = $"{error: 0.0000}";
        }
    }
}
