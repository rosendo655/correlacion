﻿namespace Correlacion.Forms
{
    partial class FileLoaderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_archivo = new System.Windows.Forms.Button();
            this.tbRuta = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.fileDialog = new System.Windows.Forms.OpenFileDialog();
            this.seleccionColumnaA = new System.Windows.Forms.ComboBox();
            this.seleccionColumnaB = new System.Windows.Forms.ComboBox();
            this.label = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.plotButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.seleccionFormula = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.seleccion_ajuste = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_archivo
            // 
            this.btn_archivo.Location = new System.Drawing.Point(13, 13);
            this.btn_archivo.Name = "btn_archivo";
            this.btn_archivo.Size = new System.Drawing.Size(75, 23);
            this.btn_archivo.TabIndex = 0;
            this.btn_archivo.Text = "Archivo";
            this.btn_archivo.UseVisualStyleBackColor = true;
            this.btn_archivo.Click += new System.EventHandler(this.btn_archivo_Click);
            // 
            // tbRuta
            // 
            this.tbRuta.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRuta.Enabled = false;
            this.tbRuta.Location = new System.Drawing.Point(94, 15);
            this.tbRuta.Name = "tbRuta";
            this.tbRuta.Size = new System.Drawing.Size(670, 20);
            this.tbRuta.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(13, 53);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(581, 385);
            this.dataGridView1.TabIndex = 2;
            // 
            // fileDialog
            // 
            this.fileDialog.FileName = "openFileDialog1";
            // 
            // seleccionColumnaA
            // 
            this.seleccionColumnaA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.seleccionColumnaA.DisplayMember = "Nombre";
            this.seleccionColumnaA.FormattingEnabled = true;
            this.seleccionColumnaA.Location = new System.Drawing.Point(601, 69);
            this.seleccionColumnaA.Name = "seleccionColumnaA";
            this.seleccionColumnaA.Size = new System.Drawing.Size(163, 21);
            this.seleccionColumnaA.TabIndex = 3;
            this.seleccionColumnaA.SelectedIndexChanged += new System.EventHandler(this.SelectedIndexChanged);
            // 
            // seleccionColumnaB
            // 
            this.seleccionColumnaB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.seleccionColumnaB.DisplayMember = "Nombre";
            this.seleccionColumnaB.FormattingEnabled = true;
            this.seleccionColumnaB.Location = new System.Drawing.Point(600, 125);
            this.seleccionColumnaB.Name = "seleccionColumnaB";
            this.seleccionColumnaB.Size = new System.Drawing.Size(163, 21);
            this.seleccionColumnaB.TabIndex = 4;
            this.seleccionColumnaB.SelectedIndexChanged += new System.EventHandler(this.SelectedIndexChanged);
            // 
            // label
            // 
            this.label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(601, 53);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(82, 13);
            this.label.TabIndex = 5;
            this.label.Text = "Primer columna:";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(600, 109);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Segunda columna:";
            // 
            // plotButton
            // 
            this.plotButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.plotButton.Enabled = false;
            this.plotButton.Location = new System.Drawing.Point(608, 415);
            this.plotButton.Name = "plotButton";
            this.plotButton.Size = new System.Drawing.Size(156, 23);
            this.plotButton.TabIndex = 7;
            this.plotButton.Text = "Calcular";
            this.plotButton.UseVisualStyleBackColor = true;
            this.plotButton.Click += new System.EventHandler(this.plotButton_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(600, 163);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Formula de correlacion:";
            // 
            // seleccionFormula
            // 
            this.seleccionFormula.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.seleccionFormula.DisplayMember = "Nombre";
            this.seleccionFormula.FormattingEnabled = true;
            this.seleccionFormula.Location = new System.Drawing.Point(600, 179);
            this.seleccionFormula.Name = "seleccionFormula";
            this.seleccionFormula.Size = new System.Drawing.Size(163, 21);
            this.seleccionFormula.TabIndex = 8;
            this.seleccionFormula.SelectedIndexChanged += new System.EventHandler(this.SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(601, 215);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Seleccion de Ajuste";
            // 
            // seleccion_ajuste
            // 
            this.seleccion_ajuste.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.seleccion_ajuste.DisplayMember = "Nombre";
            this.seleccion_ajuste.FormattingEnabled = true;
            this.seleccion_ajuste.Location = new System.Drawing.Point(601, 231);
            this.seleccion_ajuste.Name = "seleccion_ajuste";
            this.seleccion_ajuste.Size = new System.Drawing.Size(163, 21);
            this.seleccion_ajuste.TabIndex = 10;
            this.seleccion_ajuste.SelectedIndexChanged += new System.EventHandler(this.SelectedIndexChanged);
            // 
            // FileLoaderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(776, 450);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.seleccion_ajuste);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.seleccionFormula);
            this.Controls.Add(this.plotButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label);
            this.Controls.Add(this.seleccionColumnaB);
            this.Controls.Add(this.seleccionColumnaA);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.tbRuta);
            this.Controls.Add(this.btn_archivo);
            this.Name = "FileLoaderForm";
            this.Text = "FileLoaderForm";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_archivo;
        private System.Windows.Forms.TextBox tbRuta;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.OpenFileDialog fileDialog;
        private System.Windows.Forms.ComboBox seleccionColumnaA;
        private System.Windows.Forms.ComboBox seleccionColumnaB;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button plotButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox seleccionFormula;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox seleccion_ajuste;
    }
}