﻿using Correlacion.Control;
using Correlacion.Extensions;
using Correlacion.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Correlacion.Forms
{
    public partial class FileLoaderForm : Form
    {
        private Tabla<float> tabla;
        public FileLoaderForm()
        {
            InitializeComponent();
            seleccionFormula.Items.AddRange(FormulaCorrelacion.Formulas);
            seleccion_ajuste.Items.AddRange(Regresiones.TodasRegresiones);
        }

        private bool ActivateButton => 
            seleccionColumnaA.SelectedItem != null &&
            seleccionFormula.SelectedItem != null &&
            seleccion_ajuste.SelectedItem != null &&
            seleccionColumnaB.SelectedItem != null;

        private void UpdateForm() => plotButton.Enabled = ActivateButton;
        
        private void btn_archivo_Click(object sender, EventArgs e)
        {
            try
            {
                CargarArchivo();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Formato de archivo incorrecto\n" + ex.Message);
            }
        }
        private void SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateForm();
        }
        private void plotButton_Click(object sender, EventArgs e)
        {
            try
            {
                Plot();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error al calcular los resultados\n" + ex.Message);
            }
        }

        private void CargarArchivo()
        {
            fileDialog.ShowDialog();
            if (fileDialog.FileName != null)
            {
                tbRuta.Text = fileDialog.FileName;

                var stream = fileDialog.OpenFile();

                string str = FileManager.FileContent(stream);

                tabla = FileParser.ParseFile(str, ParsingFunctions.ParseFloatTable);

                dataGridView1.Columns.Clear();
                dataGridView1.Rows.Clear();
                tabla.Columnas.DoToAll(col => dataGridView1.Columns.Add(col.Nombre, col.Nombre));
                tabla.StringFormatValues.DoToAll(row => dataGridView1.Rows.Add(row));

                seleccionColumnaA.Items.Clear();
                seleccionColumnaB.Items.Clear();

                seleccionColumnaA.Items.AddRange(tabla.Columnas.Select(s => s).ToArray());
                seleccionColumnaB.Items.AddRange(tabla.Columnas.Select(s => s).ToArray());
            }

            UpdateForm();
        }
        private void Plot()
        {
            PlotForm plotForm = new PlotForm
                (
                    tabla,
                    new[] 
                    { 
                        seleccionColumnaB.SelectedItem as Columna, 
                        seleccionColumnaA.SelectedItem as Columna 
                    },
                    seleccionFormula.SelectedItem as FormulaCorrelacion,
                    seleccion_ajuste.SelectedItem as IMetodoRegresion
                );
            this.Enabled = false;
            plotForm.ShowDialog();
            this.Enabled = true;
        }
    }
}
