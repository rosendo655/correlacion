﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Correlacion.Extensions
{
    public static class IntExtensions
    {
        public static void DoTimes(this int num, Action<int> action)
        {
            for (int i = 0; i < num; i++) action(i);
        }
    }
}
