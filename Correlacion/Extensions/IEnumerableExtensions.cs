﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Correlacion.Extensions
{
    public static class IEnumerableExtensions
    {
        public static void DoToAll<T>(this IEnumerable<T> items, Action<T> DoItem)
        {
            foreach (var item in items)
            {
                DoItem(item);
            }
        }
    }
}
