﻿using Correlacion.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Correlacion.Control
{
    public class FileParser
    {
        private static string[] Lines(string str) => str.Split('\n');
        private static string[] Values(string str) => str.Split(',');
        private static string[][] Table(string str) => Lines(str).Select(s => Values(s)).ToArray();

        public static T ParseFile<T>(string fileContent, Func<string[][],T> parseFunction)
        {
            T parsedObject = parseFunction(Table(fileContent));
            return parsedObject;
        }
    }
}
