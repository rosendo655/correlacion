﻿using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Axes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Correlacion.Model;

namespace Correlacion.Control
{
    public class PlotModelGenerator
    {
        public static PlotModel ScatterModel(Tabla<float> table,IEnumerable<Columna> columnas, IFuncionRegresion plotFunction)
        {
            PlotModel model = new PlotModel();

            int xCol = columnas.First().Numero;
            int yCol = columnas.Last().Numero;

            float xMax = table.Filas.Max(m => m[xCol]);
            float xMin = table.Filas.Min(m => m[xCol]);

            

            model.Axes.Add(new LinearAxis() { Position = AxisPosition.Left, Title = table.Columnas[xCol].Nombre });
            model.Axes.Add(new LinearAxis() { Position = AxisPosition.Bottom ,Title = table.Columnas[yCol].Nombre});

            var scaterSerie = new ScatterSeries() { Title = "Real" };
            var scaterSerieEstimacion = new ScatterSeries() { MarkerType = MarkerType.Circle ,Title = "Estimado"};
            var functinSerie = new FunctionSeries(x => plotFunction.PlotFunction((float)x), xMin, xMax, 5) { Title = plotFunction.StringFuntionDescription}; 
            
            scaterSerie.Points.AddRange(table.Filas.Select(s => new ScatterPoint(s[xCol], s[yCol])));
            scaterSerieEstimacion.Points.AddRange(table.Filas.Select(s => new ScatterPoint(s[xCol], plotFunction.PlotFunction(s[xCol]))));
            


            model.Series.Add(functinSerie);
            model.Series.Add(scaterSerie);
            model.Series.Add(scaterSerieEstimacion);

            return model;
        }
    }
}
