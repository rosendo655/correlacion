﻿using Correlacion.Extensions;
using Correlacion.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Correlacion.Control
{
    public class Calculos
    {
        public static float CalculoPearson(Tabla<float> tabla, IEnumerable<Columna> columnas)
        {

            int columnaX = columnas.First().Numero;
            int columnaY = columnas.Last().Numero;

            float n = tabla.Filas.Count();

            float xsumatoria = tabla.Filas.Sum(s => s.Calc(vals => vals[columnaX]));
            float x2sumatoria = (float)tabla.Filas.Sum(s => s.Calc(vals => Math.Pow(vals[columnaX], 2)));

            float ysumatoria = tabla.Filas.Sum(s => s.Calc(vals => vals[columnaY]));
            float y2sumatoria = (float)tabla.Filas.Sum(s => s.Calc(vals => Math.Pow(vals[columnaY], 2)));

            float xysumatoria = tabla.Filas.Sum(s => s.Calc(vals => vals[columnaX] * vals[columnaY]));

            float numerador = (n * xysumatoria) - (xsumatoria * ysumatoria);
            float denominadorX = (float)Math.Sqrt(((n * x2sumatoria) - Math.Pow(xsumatoria, 2)));
            float denominadorY = (float)Math.Sqrt(((n * y2sumatoria) - Math.Pow(ysumatoria, 2)));

            float coeficiente = numerador / (denominadorX * denominadorY);

            return coeficiente;

        }

        public static float CalculoSpearman(Tabla<float> tabla, IEnumerable<Columna> columnas)
        {
            int columnaX = columnas.First().Numero;
            int columnaY = columnas.Last().Numero;
           
            float n = tabla.Filas.Count;
            float orden = 1;

            var iorden = tabla.Filas.OrderBy(o => o[columnaX]).Select(s => new OrdenValor<float, float>(s.Numero, orden++, s[columnaX])).ToList();
            orden = 1;
            var torden = tabla.Filas.OrderBy(o => o[columnaY]).Select(s => new OrdenValor<float, float>(s.Numero, orden++, s[columnaY])).ToList();

            var igroup = iorden.GroupBy(g => g.Valor).ToList();

            var tgroup = torden.GroupBy(g => g.Valor).ToList();

            igroup.DoToAll(
                group =>
                {
                    float promedioLugares = group.Average(a => a.Orden);
                    group.DoToAll(g => g.Orden = promedioLugares);
                }
                );

            tgroup.DoToAll(
                group =>
                {
                    float promedioLugares = group.Average(a => a.Orden);
                    group.DoToAll(g => g.Orden = promedioLugares);
                }
                );

            float d2sumatoria = 0;

            iorden = iorden.OrderBy(o => o.Fila).ToList();
            torden = torden.OrderBy(o => o.Fila).ToList();

            iorden.Count.DoTimes
                (
                    i =>
                    {
                        d2sumatoria += (float)Math.Pow((iorden[i].Orden - torden[i].Orden), 2.0);
                    }
                );

            float numerador = 6f * d2sumatoria;
            float denominador = n * ((float)Math.Pow(n, 2.0) - 1);

            float coeficiente = 1 - (numerador / denominador);
            return coeficiente;
        } 


        public static FuncionRegresionLineal FuncionLineal(Tabla<float> tabla, IEnumerable<Columna> columnas)
        {
            int columnaX = columnas.First().Numero;
            int columnaY = columnas.Last().Numero;

            float n = tabla.Filas.Count;
            float xySumatoria = tabla.Filas.Sum(s => s.Calc(vals => vals[columnaX] * vals[columnaY]));

            float xSumatoria = tabla.Filas.Sum(s => s.Calc(vals => vals[columnaX]));
            float ySumatoria = tabla.Filas.Sum(s => s.Calc(vals => vals[columnaY]));

            float x2Sumatoria = tabla.Filas.Sum(s => s.Calc(vals => vals[columnaX] * vals[columnaX]));

            float pendiente = ((n * xySumatoria) - (xSumatoria * ySumatoria)) / ((n * x2Sumatoria) - (xSumatoria * xSumatoria));

            float interseccion = ((ySumatoria * x2Sumatoria) - (xSumatoria * xySumatoria)) / ((n * x2Sumatoria) - (xSumatoria * xSumatoria));

            return new FuncionRegresionLineal { b = interseccion, m = pendiente , PlotFunction = f => (f*pendiente) + interseccion , StringFuntionDescription = $"{pendiente:0.000}x + ({interseccion:0.000})" };
        }

        public static float Error(Tabla<float> tabla , IEnumerable<Columna> columnas , Func<float,float> funcionAjuste)
        {
            int colX = columnas.First().Numero;
            int colY = columnas.Last().Numero;

            float sumatoriaDiferencia2 = tabla.Filas.Sum(s => s.Calc(vals => (float)Math.Pow(vals[colY] - funcionAjuste(vals[colX]), 2.0)));

            return sumatoriaDiferencia2;
        }

        public static string StringFuncionAjuste(Func<float, float> FuncionAjuste)
        {
            string interseccion = $"{FuncionAjuste(0):0.0000}";
            string pendiente = $"{ FuncionAjuste(1) - FuncionAjuste(0):0.0000}";

            return $"F(x) = {pendiente}x + ({interseccion})";
        }
    }
}
