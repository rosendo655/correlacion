﻿using Correlacion.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Correlacion.Control
{
    public class ParsingFunctions
    {
        private static Tabla<T> ParseTable<T>(string[][] data,Func<string,T> parseFunction, Func<T,string> dataStringFunction)
        {
            return Tabla<T>.Parse(data,parseFunction,dataStringFunction);
        }

        public static Tabla<float> ParseFloatTable(string[][] data)
        {
            return ParseTable(data, str => float.Parse(str), flt => $"{flt:0.00}");
        }

    }
}
