﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Correlacion.Control
{
    public class FileManager
    {
        public static string FileContent(Stream str)
        {
            StringBuilder builder = new StringBuilder();
            int byteRead;
            while((byteRead = str.ReadByte())!= -1)
            {
                builder.Append((char)byteRead);
            }
            return builder.ToString();
        }
    }
}
