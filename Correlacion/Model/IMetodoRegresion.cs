﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Correlacion.Model
{
    public interface IMetodoRegresion
    {
        string Nombre { get; set; }

        Func<Tabla<float>, IEnumerable<Columna>, IFuncionRegresion> FuncionRegresion { get; set; }
    }
}
