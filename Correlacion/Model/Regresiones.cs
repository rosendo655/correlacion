﻿using Correlacion.Control;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Correlacion.Model
{
    public class Regresiones
    {
        public static IMetodoRegresion[] TodasRegresiones =>
            new[]
            {
                regresionLineal
            };
        private static RegresionLineal regresionLineal = new RegresionLineal
        {
            Nombre = "Lineal",
            FuncionRegresion = Calculos.FuncionLineal
        };
    }
}
