﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Correlacion.Model
{
    public class FuncionRegresion : IFuncionRegresion
    {
        public string StringFuntionDescription { get; set; }
        public Func<float, float> PlotFunction { get; set; }
    }
}
