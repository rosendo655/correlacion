﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Correlacion.Model
{
    public interface IFuncionRegresion
    {
        string StringFuntionDescription { get; set; }
        Func<float,float> PlotFunction { get; set; }
    }
}
