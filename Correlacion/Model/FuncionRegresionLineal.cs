﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Correlacion.Model
{
    public class FuncionRegresionLineal : IFuncionRegresion
    {
        public string StringFuntionDescription { get; set; }
        public float m { get; set; }
        public float b { get; set; }
        public Func<float, float> PlotFunction { get; set; }

        
    }
}
