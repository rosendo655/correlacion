﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Correlacion.Model
{
    public class Fila<T>
    {
        public int Numero { get; set; }
        public List<T> Valores { get; set; }
        public T this[int num] => Valores[num];

        public TOut Calc<TOut>(Func<List<T>, TOut> calcFunction) => calcFunction(Valores);
        
        
    }
}
