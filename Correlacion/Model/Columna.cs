﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Correlacion.Model
{
    public class Columna
    {
        public string Nombre { get; set; }
        public int Numero { get; set; }
    }
}
