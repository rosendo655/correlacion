﻿using Correlacion.Control;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Correlacion.Model
{
    public class FormulaCorrelacion
    {
        private static FormulaCorrelacion Pearson = new FormulaCorrelacion("Pearson", Calculos.CalculoPearson);
        private static FormulaCorrelacion Spearman = new FormulaCorrelacion("Spearman",Calculos.CalculoSpearman);

        public static FormulaCorrelacion[] Formulas => new[] { Pearson, Spearman };
        

        public string Nombre { get; private set; }
        public Func<Tabla<float>, IEnumerable<Columna>, float> CalculoIndiceRelacion { get; private set; }

        private FormulaCorrelacion(string nombre, Func<Tabla<float>,IEnumerable<Columna>, float> calculoIndice)
        {
            Nombre = nombre;
            CalculoIndiceRelacion = calculoIndice;
        }
    }
}
