﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Correlacion.Model
{
    public class OrdenValor<TOrden,TValor>
    {
        public int Fila { get; set; }
        public TOrden Orden { get; set; }
        public TValor Valor { get; set; }

        public OrdenValor(int fila,TOrden orden, TValor valor)
        {
            Orden = orden;
            Fila = fila;
            Valor = valor;
        }
    }
}
