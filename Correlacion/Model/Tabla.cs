﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Correlacion.Model
{
    public class Tabla<T>
    {
        public List<Columna> Columnas { get; set; }
        public List<Fila<T>> Filas { get; set; }
        public List<string[]> StringFormatValues => Filas.Select(s => s.Valores.Select(val => dataStringFunc(val)).ToArray() ).ToList();
        private Func<T, string> dataStringFunc = data => data.ToString();

        public Tabla()
        {

        }
        public Tabla
            (
            IEnumerable<Columna> columnas,
            IEnumerable<Fila<T>> filas,
            Func<T, string> dataString
            )
        {
            Columnas = columnas.ToList();
            Filas = filas.ToList();
            dataStringFunc = dataString;
        }


        public T this[int col,int fil] => Filas[fil][col];

        public T[] this[int col] => Filas.Select(s => s.Valores[col]).ToArray();

        public static Tabla<T> Parse(string [][] data , Func<string,T> parsingFunction, Func<T,string> dataStringFunction)
        {
            string[] headers = data[0];
            string[][] rows = data.Skip(1).ToArray();
            int columnCounter = 0;
            
            var columnas = headers.Select(s => new Columna { Nombre = s, Numero = columnCounter++ }).ToList();
            int rowCounter = 0;
            var filas = rows.Select(row =>
           {
               return new Fila<T> { Numero = rowCounter++, Valores = row.Select(val => parsingFunction(val)).ToList() };
           }).ToList();


            return new Tabla<T>(columnas, filas, dataStringFunction);
        }


    }
}
